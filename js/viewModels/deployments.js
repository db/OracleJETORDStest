/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * deployments module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojtable'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function deploymentsContentViewModel() {
        var self = this;
        self.data = ko.observableArray();
        var clientId = 'EmTtO8WcgNOywZaCs7QPDQ..';
        var state = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        var ordsOauth2AuthBaseUrl = "https://devapex.cern.ch/ords/devdb11/cerndb_mwctl_a_01_dev/oauth/auth";
        var ordsOauth2AuthUrl = ordsOauth2AuthBaseUrl + "?response_type=token&client_id=" + clientId + "&state=" + state;
        var deploymentsUrl = "https://devapex.cern.ch/ords/devdb11/cerndb_mwctl_a_01_dev/deployment/";
        $(document).ready(function () {
            $.ajax({
                url: deploymentsUrl,
                type: 'GET',
                dataType: 'json',
                success: function (settings) {
                    console.log(settings);
                    $.each(settings.items, function () {
                        self.data.push({
                            user_resource_id: this.user_resource_id,
                            creation_date: this.creation_date,
                            name: this.name,
                            version: this.version
                        });
                    });
                },
                error: function () {
                    window.location = ordsOauth2AuthUrl;
                },
                beforeSend: function (xhr) {
                    setHeader(xhr);
                }
            });
        });
        self.dataSource = new oj.ArrayTableDataSource(
                self.data,
                {idAttribute: 'user_resource_id'});
    }

    function setHeader(xhr) {
        var token = getUrlVars()["token"];
        if (token !== undefined) {
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        } else {
            console.log("No token in the request");
        }
    }

    function randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i)
            result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    return deploymentsContentViewModel;
});
