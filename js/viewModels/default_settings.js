/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * default_settings module
 */
define(['ojs/ojcore', 'knockout', 'ojs/ojtable'
], function (oj, ko) {
    /**
     * The view model for the main content view template
     */
    function default_settingsContentViewModel() {
        var self = this;
        self.data = ko.observableArray();
        var url = "https://devapex.cern.ch/ords/devdb11/cerndb_mwctl_a_01_dev/defaultsetting/";
        $.getJSON(url).
                then(function (settings) {
                    $.each(settings.items, function () {
                        self.data.push({
                            default_setting_id: this.default_setting_id,
                            is_default: this.is_default,
                            setting_name: this.setting_name,
                            setting_value: this.setting_value
                        });
                    });
                });
        self.dataSource = new oj.ArrayTableDataSource(
                self.data,
                {idAttribute: 'default_setting_id'}
        );
    }

    return default_settingsContentViewModel;
});
