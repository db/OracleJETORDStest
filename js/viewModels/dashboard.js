/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your dashboard ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojtable'],
        function (oj, ko, $) {

            function DashboardViewModel() {
                var self = this;
                self.currentSelection = ko.observable();
                var deptArray = [
                    {DepartmentId: 1001, DepartmentName: 'ADFPM 1001 neverending', LocationId: 200, ManagerId: 300},
                    {DepartmentId: 556, DepartmentName: 'BB', LocationId: 200, ManagerId: 300},
                    {DepartmentId: 10, DepartmentName: 'Administration', LocationId: 200, ManagerId: 300}
                ];
                self.datasource = new oj.ArrayTableDataSource(deptArray, {idAttribute: 'DepartmentId'});
                self.selectedItem = ko.pureComputed(function (ctx) {
                    if (self.currentSelection()) {
                        var value = deptArray[self.currentSelection()[0].startIndex.row];
                        return value.DepartmentName;
                    } else {
                        return "Nothing selected";
                    }
                });
            }
            return new DashboardViewModel();
        }
);
