/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your customer ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojtree'],
        function (oj, ko, $)
        {
            $(
                    function ()
                    {
                        ko.applyBindings(null, document.getElementById('tree'));

                        $("#tree").on("ojoptionchange", function (e, ui) {
                            if (ui.option == "selection") {
                                // show selected nodes
                                var selected = _arrayToStr(ui.value);
                                $("#results").text("id = " + selected);
                            }
                        });
                    }
            );
        });

function  getJson(node, fn)       // get local json
{
    var data = [
        {
            "title": "News",
            "attr": {"id": "news"}
        },
        {
            "title": "Blogs",
            "attr": {"id": "blogs"},
            "children": [{"title": "Today",
                    "attr": {"id": "today"}
                },
                {"title": "Yesterday",
                    "attr": {"id": "yesterday"}
                },
                {"title": "Archive",
                    "attr": {"id": "archive"}
                }
            ]
        },
        {
            "title": "Links",
            "attr": {"id": "links"},
            "children": [{"title": "Oracle",
                    "attr": {"id": "oracle"}
                },
                {"title": "IBM",
                    "attr": {"id": "ibm"}
                },
                {"title": "Microsoft",
                    "attr": {"id": "ms"},
                    "children": [{"title": "USA",
                            "attr": {"id": "msusa"},
                            "children": [{"title": "North",
                                    "attr": {"id": "msusanorth"}
                                },
                                {"title": "South",
                                    "attr": {"id": "msusasouth"}
                                },
                                {"title": "East",
                                    "attr": {"id": "msusaeast"}
                                },
                                {"title": "West",
                                    "attr": {"id": "msusawest"}
                                }
                            ]
                        },
                        {"title": "Europe",
                            "attr": {"id": "msuerope"}
                        },
                        {"title": "Asia",
                            "attr": {"id": "msasia"},
                            "children": [{"title": "Japan",
                                    "attr": {"id": "asiajap"}
                                },
                                {"title": "China",
                                    "attr": {"id": "asiachina"}
                                },
                                {"title": "India",
                                    "attr": {"id": "asiaindia"}
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ];

    fn(data);  // pass to ojTree using supplied function
}
;

// Convert a jQuery list of html element nodes to string containing node id's.
function _arrayToStr(arr)
{
    var s = "";
    $.each(arr, function (i, val)
    {
        if (i) {
            s += ", ";
        }
        s += $(arr[i]).attr("id");
    });

    return s;
}
;
