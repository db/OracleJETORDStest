/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/*
 * Your incidents ViewModel code goes here
 */
define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojtree'],
        function (oj, ko, $) {

            function IncidentsViewModel() {
                var self = this;
                self.currentSelection = ko.observable();
                self.datasource = [
                    {"title": "Home", "attr": {"id": "home"},
                        "children": [
                            {"title": "Overview", "attr": {"id": "overview"}},
                            {"title": "Section1", "attr": {"id": "section1"}},
                            {"title": "Section2", "attr": {"id": "section2"}}
                        ]
                    },
                    {"title": "News", "attr": {"id": "news"}},
                ];
                self.selectedItem = ko.pureComputed(function (ctx) {
                    if (self.currentSelection()) {
                        var value = self.currentSelection()[0]['id'];
                        return value;
                    } else {
                        return "Nothing selected";
                    }
                });
            }

            /*
             * Returns a constructor for the ViewModel so that the ViewModel is constrcuted
             * each time the view is displayed.  Return an instance of the ViewModel if
             * only one instance of the ViewModel is needed.
             */
            return new IncidentsViewModel();
        }
);
